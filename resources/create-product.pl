#!/usr/bin/perl
#
# Create a product's webpage from a `product' file, a screenshot and an icon.
#
# Syntax
# ======
#   create-product product.txt icon.svg screenshot.png...
#
# Product file
# ============
#   A product file consists of three sections:
#     1. A two-line header
#     2. A series of key-value metadata pairs
#     3. Some [Markdown](http://daringfireball.net/projects/markdown/syntax) text.
#
#   Each section is separated by one or more more blank lines.
#
# Heading
# -------
#   The two-line header consists of the product name, followed by its byline.
#
# Metadata
# --------
#   The metadata defines the sidebar external links for more information about the
#   product. All values are currently links. The keys can be:
#
#      - `Symbian`: where to download the Symbian version
#      - `Diablo`: where to download for N8x0
#      - `Fremantle`: where to download for N900
#      - `Harmattan`: where to download for N9(50)
#      - `Source`: source repository
#      - `Bugs`: bug tracker
#
# Description
# -----------
#   The description should be simple [Markdown](http://daringfireball.net/projects/markdown/syntax).
#
# --------------------------------------------------------------
# Copyright (c) Jaffa Software 2011. All rights reserved.

use strict;
use warnings;
use HTML::Entities;
use Text::Markdown;
use Image::Magick;
use FindBin;

my $resDir = "$FindBin::Bin/product";

# -- Read input...
#
my $product = shift or die "syntax: $0 product.txt icon screenshot.png...\n";
my $icon    = shift or die "syntax: $0 product.txt icon screenshot.png...\n";
my @sshots  = @ARGV;

# -- Information on the product...
#
my $title       = '<unknown>';
my $subtitle    = '';
my %metadata    = ();
my $description = '';

# -- Read it in...
#
open(IN, "<$product") or die "Unable to read '$product': $!\n";
chomp($title = <IN>);
chomp($subtitle = <IN>);
<IN> if $subtitle !~ /^\s*$/;

my $pid = lc($title);
$pid =~ s/\W+//g;

while (<IN>) {
  last if /^\s*$/;
  my ($key, $value) = /^(\w+)\s*:\s*(.*?)[\r\n\s]*$/;
  $metadata{$key} = encode_entities($value);
}

while (<IN>) {
  $description .= $_;
}
close(IN);

# -- Start outputting things...
#
my $targetDir = "$resDir/../../$pid";
mkdir $targetDir;

# -- Generate the screenshot...
#
if (@sshots) {
  my $sshot = Image::Magick->new;
  $sshot->Read($sshots[0]);
 
  my $highlight = Image::Magick->new;
  $highlight->Read("$resDir/highlight2.png");

  my ($w, $h) = $sshot->Get('width', 'height');
  $highlight->Resize(geometry => "${w}x${h}!");
  $sshot->Composite(image => $highlight, compose => 'HardLight');
  $sshot->Write("$targetDir/temp.png");
#  system('convert', $sshots[0], '-compose', 'hardlight',
#                    '(', "$resDir/highlight2.png", '-resize', "${w}x${h}!", ')',
#                    '-composite',
#                    'temp.png');

  # Produce appropriate landscape/portrait image
  if ($w < $h) {
    ### TODO Portrait
  } else {
     system('convert', '(', "$resDir/1200-nokia_n9_06.jpg", ')',
                       '(', "$targetDir/temp.png", '-matte', '-virtual-pixel', 'transparent',
                            '+distort', 'Perspective',
                            '0,0 301,351  0,480 393,846  854,480 1019,781  854,0, 912,409', ')',
                       '-layers', 'merge',
                       '-crop', '1018x904+102+280',
                       '-resize', '450x400',
                       "$targetDir/$pid-sshot.jpg");
  }
  unlink "$targetDir/temp.png";
}

# -- Generate the icon...
#
if ($icon =~ /\.svg$/i) {
  system('rsvg', '--width=128', '--height=128', $icon, "$targetDir/$pid-128.png");
} else {
  my $image = Image::Magick->new;
  $image->Read($icon);
  $image->Resize(geometry => '128x128');
  $image->Write("$targetDir/$pid-128.png");
}

# -- Output it...
#
encode_entities($title);
encode_entities($subtitle);
$description = Text::Markdown::Markdown($description);

open(STDOUT, ">$targetDir/index.html") or die "Can't redirect stdout: $!\n";
print <<EO1;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" type="image/x-icon" href="../resources/favicon.ico" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Open+Sans+Condensed:300" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../resources/style.css" />
<title>$title - Jaffa Software</title>
</head>
<body id="product"><div id="main">
<div id="header" style="background-image: url($pid-sshot.jpg)">
<h1>$title</h1>
<h2>$subtitle</h2>
</div>

<div id="notheader">
<div class="sidebar">
<h3>Available on:</h3>
<ul>
EO1

print <<EO2 if $metadata{'Symbian'};
<li class="symbian"><a href="$metadata{Symbian}">Symbian</a></li>
EO2

print <<EO3 if $metadata{'Diablo'};
<li class="maemo"><a href="$metadata{Diablo}">Maemo 4</a></li>
EO3

print <<EO4 if $metadata{'Fremantle'};
<li class="maemo"><a href="$metadata{Fremantle}">Maemo 5</a></li>
EO4

print <<EO5 if $metadata{'Harmattan'};
<li class="harmattan"><a href="$metadata{Harmattan}">Nokia N9</a></li>
EO5

if ($metadata{'Source'}) {
  my ($site)  = $metadata{'Source'} =~ m!https?://(?:www\.)?(.*?)/!;
  my ($class) = $site =~ m!(?:.*\.)?([^\.]+)\.[^\.]+$!;
  print <<EO6;
<li class="$class"><a href="$metadata{Source}">$site</a></li>
EO6
}

print "</ul>\n</div>\n";

print <<EO7 if $metadata{'Bugs'};
<div class="sidebar">
<h3><a href="$metadata{Bugs}">Raise a bug</a></h3>
</div>
EO7

print <<E08;
<div class="sidebar">
<h3><a href="https://www.paypal.com/xclick/business=sales%40jaffasoft.co.uk&amp;item_name=Jaffa+Software+donation&amp;no_shipping=1&amp;no_note=1&amp;tax=0&amp;currency_code=GBP">Donate</a></h3>
</div>

<div class="sidebar">
<h3><a href="mailto:info\@jaffasoft.co.uk">Contact</a></h3>
</div>

<div id="content">
$description
</div>

<div class="sidebar icon"><img src="$pid-128.png" width="128" height="128" alt="[$title logo]" /></div>
</div>
</div>

<div id="footer">
<div class="column first"></div>
<div class="column first">
<h3>Mobile apps</h3>
<ul>
<li><a href="/m/bedside/">Bedside</a></li>
<li><a href="http://hermes.garage.maemo.org/">Hermes</a></li>
<li><a href="http://maemo.org/downloads/product/Maemo5/attitude/">Attitude</a></li>
</ul>
</div>

<div class="column">
<h3>Desktop apps</h3>
<ul>
<li><a href="http://mediautils.garage.maemo.org/tablet-encode.html">tablet-encode</a></li>
<li><a href="http://bleb.org/software/maemo/#770Flasher">770Flasher.app</a></li>
<li><a href="http://www.jaffasoft.co.uk/">RISC OS</a></li>
</ul>
</div>

<div class="column">
<h3>Contact</h3>
<ul>
<li><a href="mailto:info\@jaffasoft.co.uk">Email</a></li>
<li><a href="http://twitter.com/jaffa2">Twitter</a></li>
<li><a href="http://www.maemopeople.org/index.php/jaffa">Blog</a></li>
<li><a href="http://maemo.org/profile/view/jaffa/">maemo.org</a></li>
</ul>
</div>

<div id="trail">
<p>Copyright &copy; <a href="mailto:info\@jaffasoft.co.uk">Jaffa Software</a> 2011</p>
</div>
</div>
</body>
</html>
E08

